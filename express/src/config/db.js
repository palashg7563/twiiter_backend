import mongoose from 'mongoose';
import { DB_URL } from "./index";
import { isDEV } from "./index"

//setting the Promise of the global promise
mongoose.Promise = global.Promise;

//debugging is on for only for the development process
if (isDEV) {
  mongoose.set('debug', true);
}

try {
  mongoose.connect(DB_URL, {
    useNewUrlParser: true,
  });
} catch (error) {
  mongoose.connect(DB_URL, {
    useNewUrlParser: true,
  });
}

mongoose.connection
  .once('open', () => console.log('MongoDB is running'))
  .on('error', (e) => { throw e; });
